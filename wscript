#! /usr/bin/env python3
# encoding: utf-8

top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_cxx')

def configure(conf):
    conf.load('compiler_cxx')
    conf.env.INCLUDES_pybind11 = ['/opt/anaconda3/include/python3.7m','/opt/anaconda3/include']
    conf.env.LIBPATH_pybind11 = ['/opt/anaconda3/lib']
    conf.env.LIB_pybind11 = 'python3.7m'

def build(bld):
    bld.program(source='src/main.cpp', target='app', use='pybind11')