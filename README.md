# What is this test ?
It's a basic sample code including a python interpreter into a C++ main process using pybind11. The interpreter will just print HELLO WORLD !, create a numpy array using np.ones and print the first value of the array, i.e. 1.

This test was made after some struggles with doing such things inside a RTC Tk Data task :

I finally managed to make it compile adding pybind11 in the rtctk root wscript :

```python
def configure(cnf):
    pkgs = 'log4cplus xerces-c cpp-netlib-uri cfitsio yaml-cpp backtrace numa rt pybind11'
    ...

    cnf.check_cfg(package='pybind11', uselib_store='PYBIND11', args='--cflags')
```

And in the exampleDataTask wscript :

```python
declare_cprogram(target='rtctkExampleDataTask',
                use=['componentFramework.rtcComponent',
                     'reusableComponents.dataTask',
                     '_examples.exampleTopics', 'PYBIND11'],
                features='pyext')
```

Unfortunately, I got some errors as soon as I try to use numpy in the code :

```
/opt/anaconda38/lib/python3.7/site-packages/numpy/core/_multiarray_umath.cpython-37m-x86_64-linux-gnu.so: undefined symbol: PyMemoryView_FromObject

/opt/anaconda38/lib/python3.7/site-packages/numpy/__init__.py:138: UserWarning: mkl-service package failed to import, therefore Intel(R) MKL initialization ensuring its correct out-of-the box operation under condition when Gnu OpenMP had already been loaded by Python process is not assured. Please install mkl-service package, see http://github.com/IntelPython/mkl-service
  from . import _distributor_init
```

# How to use ?
```
waf configure build
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/anaconda3/lib
./build/app
```

