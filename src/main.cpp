#include <stdio.h>
#include <iostream>
#include <pybind11/embed.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

namespace py = pybind11;
using namespace py::literals;

int main () {
    py::scoped_interpreter guard{};
    auto locals = py::dict();    
    py::exec(R"(
        import numpy as np
        print("HELLO WORLD !")
        A = np.ones(10)
    )", py::globals(), locals);
    auto A = locals["A"].cast<std::vector<float>>();
    std::cout << A[0] << std::endl;
}